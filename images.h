/*
 * images.h
 *
 *  Created on: Aug 3, 2019
 *      Author: nxp89240
 */

#ifndef IMAGES_H_
#define IMAGES_H_

#include "wiced_platform.h"

const uint8_t logo_temperature_40x40[];
const uint8_t degC_20x16[];
const uint8_t bluetooth_24x24[];
const uint8_t logo_ambient_40x40[];
const uint8_t Per_20x16[];
const uint8_t connect_24x24[];

#endif /* IMAGES_H_ */
