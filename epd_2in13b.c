/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, including source code, documentation and related
 * materials ("Software"), is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

#include <epd_2in13b.h>
#include "sparcommon.h"
#include "wiced_platform.h"

#include "wiced_hal_pspi.h"
#include "wiced_hal_gpio.h"
#include "wiced_bt_trace.h"
#include "fonts.h"
#include "image_data.h"
#include "string.h"


spi_interface_t spi1;
uint8_t gtxBuff[1024];
uint8_t Image_Buffer[EPD_HEIGHT*EPD_WIDTH];

/******************************************************************************
* Function Name: max44009_int_clean
***************************************************************************//**
* Clean interrupt status.
*
* \param  None
*
* \return None
******************************************************************************/

static uint8_t epd_2in13b_write_cmd(uint8_t value){

	uint8_t status;
	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_DS, 0);
	wiced_hal_pspi_tx_data(SPI2, 1u, &value);
	return status;
}


static uint8_t epd_2in13b_write_data(uint8_t *data , uint32_t length){

	uint8_t status;

	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_DS, 1);
	wiced_hal_pspi_tx_data(SPI2, length, data);
	return status;
}

static uint8_t epd_2in13b_write_byte(uint8_t data){

	uint8_t status;

	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_DS, 1);
	wiced_hal_pspi_tx_data(SPI2, 1, &data);
	return status;
}


void epd_2in13b_reset(){

	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_RST, 1);
	epd_delay(10000);
	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_RST, 0);
	epd_delay(10000);
	wiced_hal_gpio_set_pin_output(WICED_GPIO_PIN_RST, 1);
	epd_delay(10000);

}

/******************************************************************************
* Function Name: max44009_init
***************************************************************************//**
* Initializes the 44009 light sensor.
*
* \param max44009_user_set_t *max44009_usr_set
* Configure user structure.
*
*       uint16_t    scl_pin                     - scl pin definition
*
*       uint16_t    sda_pin                     - sda pin definition
*
*       uint16_t    irq_pin                     - pin used to receive interrupt signal from light sensor
*
*       uint8_t     irq_enable_reg_value        - irq enable register value
*
*       uint8_t     cfg_reg_value               - configuration register value
*
*       uint8_t     upper_threshold_reg_value   - upper threshold register value
*
*       uint8_t     low_threshold_reg_value     - low threshold register value
*
*       uint8_t     threshold_timer_reg_value   - delay time for asserting irq pin when light level exceeds threshold
*
* \param user_fn
* Points to the function to call when the interrupt comes .Below is the description of the arguments received by the cb.
*
*       void* user_data  - User data provided when interrupt is being registered
*                        using wiced_hal_gpio_register_pin_for_interrupt(...)
*
*       uint8_t port_pin - Number of the pin causing the interrupt
*
* \param usr_data
* Will be passed back to user_fn as-is. Typically NULL.
*
* \return None
******************************************************************************/
void epd_2in13b_init(void* usr_data)
{
	uint8_t status;
	uint8_t data[3];
	//wiced_hal_pspi_init(spi, devRole, spiPinPullConfig, spiGpioCfg, clkSpeed, endian, polarity, mode, csPin)
	//wiced_hal_pspi_init(SPI1, SPI_MASTER,INPUT_PIN_PULL_UP,SPI_PIN_CONFIG(WICED_P09,WICED_P11,WICED_P06,WICED_P17), 1000000u, SPI_MSB_FIRST, SPI_SS_ACTIVE_LOW, SPI_MODE_1,WICED_P11);

	epd_2in13b_reset();

    epd_2in13b_write_cmd(BOOSTER_SOFT_START);

    data[0]=data[1]=data[2]=0x17;
    epd_2in13b_write_data(data,3);

    epd_2in13b_write_cmd(POWER_ON);
    epd_2in13b_wait();
    epd_2in13b_write_cmd(PANEL_SETTING);
    data[0]=0x8F;
    epd_2in13b_write_data(data,1);

    epd_2in13b_write_cmd(VCOM_AND_DATA_INTERVAL_SETTING);
    data[0]=0xF7;
    epd_2in13b_write_data(data,1);//2.13inch b = 0xF7 ,2.13inch c = 0x77

    epd_2in13b_write_cmd(RESOLUTION_SETTING);

    data[0]=EPD_WIDTH;
    data[1]=EPD_HEIGHT >> 8;
    data[2]=EPD_HEIGHT & 0xFF;
    epd_2in13b_write_data(data,3);

    WICED_BT_TRACE("\n Initilization Done");
}

void epd_2in13b_clear(void)
{
	   uint32_t Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
	   uint32_t Height = EPD_HEIGHT;
	   uint8_t data[14]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	    //send black data
	   epd_2in13b_write_cmd(DATA_START_TRANSMISSION_1);
	    for (uint32_t j = 0; j < Height; j++) {
	        epd_2in13b_write_data(data,Width);
	    }
	    epd_2in13b_write_cmd(PARTIAL_OUT);

	    //send red data
	    epd_2in13b_write_cmd(DATA_START_TRANSMISSION_2);
	    for (uint32_t j = 0; j < Height; j++) {
	        epd_2in13b_write_data(data,Width);
	    }
	    epd_2in13b_write_cmd(PARTIAL_OUT);

	    epd_2in13b_DisplayON();
	    WICED_BT_TRACE("\n Clear Screen Done");
}



static void epd_setpartial(uint32_t x,uint32_t y){

	uint8_t pdata[8];

	epd_2in13b_write_cmd(PARTIAL_IN);
	epd_2in13b_write_cmd(PARTIAL_WINDOW);
	pdata[0]=(x<<3) & 0xf8;
	pdata[1]=((x+2) <<3) & 0xf8;
	pdata[2]=0;
	pdata[3]=y;
	pdata[4]=0;
	pdata[5]=y+24;
	pdata[6]=1;
	epd_2in13b_write_data(pdata, 7);
	memset(gtxBuff,0x00,72);
}

/******************************************************************************
function :	Sends the image buffer in RAM to e-Paper and displays
parameter:
******************************************************************************/
void epd_2in13b_draw_car(uint8_t x, uint8_t y)
{
    uint32_t Width, Height;
    Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
    Height = EPD_HEIGHT;
	uint8_t pdata[8];

    epd_2in13b_write_cmd(DATA_START_TRANSMISSION_1);
    for(uint8_t i=0;i<10;i++){

    epd_2in13b_write_data(&gImage_tpms_car[i*255],255);
    }
    epd_2in13b_write_data(&gImage_tpms_car[10*255],220);
    epd_2in13b_write_cmd(PARTIAL_OUT);

    epd_2in13b_write_cmd(DISPLAY_REFRESH);
    epd_2in13b_wait();

}


void epd_2in13b_set_digits(uint8_t x, uint8_t y, uint8_t data, uint8_t refresh)
{
    uint32_t Width, Height,len;
    Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
    Height = EPD_HEIGHT;
    uint8_t pdata[8];
    uint32_t d0,d1;

    if(data>99)data=99;

    d0= (data/10)*72;
    d1= (data%10)*72;

	epd_setpartial(x, y);
	if(data>22)
    epd_2in13b_write_cmd(DATA_START_TRANSMISSION_1);
	else
	epd_2in13b_write_cmd(DATA_START_TRANSMISSION_2);

	for(uint32_t i=0,k=d1;i<72;i=i+1,k++){
		gtxBuff[i] =~  Font24_Table[k];
	}
	epd_2in13b_write_data(gtxBuff,72);
	epd_2in13b_write_cmd(PARTIAL_OUT);

	epd_setpartial(x+2, y);
	if(data>22)
    epd_2in13b_write_cmd(DATA_START_TRANSMISSION_1);
	else
	epd_2in13b_write_cmd(DATA_START_TRANSMISSION_2);

	for(uint32_t i=0,k=d0;i<72;i=i+1,k++){
		gtxBuff[i] =~  Font24_Table[k];
	}
	epd_2in13b_write_data(gtxBuff,72);
	epd_2in13b_write_cmd(PARTIAL_OUT);

	if(refresh==1)
		epd_2in13b_refresh();

}



void epd_2in13b_refresh(){

	epd_2in13b_write_cmd(DISPLAY_REFRESH);
	epd_2in13b_wait();
}

void epd_2in13b_wait(void){

	while(0== wiced_hal_gpio_get_pin_input_status(WICED_GPIO_PIN_BSY)){
		epd_delay(1000);
	}
}

void epd_2in13b_DisplayON(void){

	epd_2in13b_write_cmd(0x12);		 //DISPLAY REFRESH
    epd_delay(2000);    //!!!The delay here is necessary, 200uS at least!!!
    epd_2in13b_wait();
}



void epd_delay(uint32_t dly){

	while(dly--);
}

